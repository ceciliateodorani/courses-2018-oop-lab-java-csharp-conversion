using System;

namespace Unibo.Oop.Mnk
{
    // TODO notice this class
    public static class SymbolsExtensions
    {
        private static readonly Random RAND = new Random();
        
        public static string ToString(this Symbols symbol)
        {
            switch (symbol)
            {
                case Symbols.CROSS:
                    return "X";
                case Symbols.ROUND:
                    return "O";
                default:
                    return "_";
            }
        }
        
        public static string GetName(this Symbols symbol)
        {
            switch (symbol)
            {
                case Symbols.CROSS:
                    return "Cross";
                case Symbols.ROUND:
                    return "Round";
                default:
                    return "Empty";
            }
        }

        public static Symbols PickRandomly(params Symbols[] symbols)
        {
            return symbols[RAND.Next(symbols.Length)];
        }

        public static Symbols PickRandomly()
        {
            return PickRandomly(Symbols.CROSS, Symbols.ROUND);
        }
        
        public static Symbols NextPlayer(this Symbols player) {
            if (player == Symbols.EMPTY)
            {
                throw new InvalidOperationException();
            }
            else if (player == Symbols.CROSS)
            {
                return Symbols.ROUND;
            }
            else
            {
                return Symbols.CROSS;
            }
        }
    }
}